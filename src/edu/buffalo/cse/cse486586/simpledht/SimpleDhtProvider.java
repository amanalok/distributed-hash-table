package edu.buffalo.cse.cse486586.simpledht;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Formatter;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.util.Log;

public class SimpleDhtProvider extends ContentProvider {

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // TODO Auto-generated method stub
		
		try {
			String node_hash = SimpleDhtActivity.node_id;
			String succ_hash = genHash(String.valueOf(SimpleDhtActivity.successor/2));
			String pred_hash = genHash(String.valueOf(SimpleDhtActivity.predecessor/2));
			String key_hash = genHash(selection);
			
			if((key_hash.compareTo(pred_hash) > 0 && key_hash.compareTo(node_hash) <= 0)
					|| (node_hash.compareTo(pred_hash) < 0 && key_hash.compareTo(pred_hash) > 0)
					|| (node_hash.compareTo(pred_hash) < 0 && key_hash.compareTo(node_hash) < 0)
					|| (node_hash.compareTo(succ_hash)==0)
					){
				getContext().deleteFile(selection);
			}
			else{
				Message msg = new Message(SimpleDhtActivity.successor, SimpleDhtActivity.avd, selection, "DELETE");
				SendTask snd = new SendTask(msg);
				snd.start();
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return 0;
    }

    @Override
    public String getType(Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // TODO Auto-generated method stub
    	String filename = values.getAsString("key");
        String value = values.getAsString("value");
        
        try {
        	String node_hash = SimpleDhtActivity.node_id;
        	String succ_hash = genHash(String.valueOf(SimpleDhtActivity.successor/2));
        	String pred_hash = genHash(String.valueOf(SimpleDhtActivity.predecessor/2));
        	String key_hash = genHash(filename);
        	
			if((key_hash.compareTo(pred_hash) > 0 && key_hash.compareTo(node_hash) <= 0)
					|| (node_hash.compareTo(pred_hash) < 0 && key_hash.compareTo(pred_hash) > 0)
					|| (node_hash.compareTo(pred_hash) < 0 && key_hash.compareTo(node_hash) < 0)
					|| (node_hash.compareTo(succ_hash)==0)
					){
				FileOutputStream fos;
				fos = getContext().openFileOutput(filename, Context.MODE_PRIVATE);
				fos.write(value.getBytes());
		        fos.close();
			}
			else{
				Message msg = new Message(SimpleDhtActivity.successor, filename, value, "INSERT");
				SendTask snd = new SendTask(msg);
			    snd.start(); 
			}
		} catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        Log.v("insert", values.toString());
        return uri;
    }

    @Override
    public boolean onCreate() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {
        // TODO Auto-generated method stub
    	
    	String[] colNames = {"key", "value"};
    	MatrixCursor cur = new MatrixCursor(colNames);
    	Log.d("Aman", "Querying for key : " + selection);
    	
		try {
			String node_hash = SimpleDhtActivity.node_id;
			String succ_hash = genHash(String.valueOf(SimpleDhtActivity.successor/2));
			String pred_hash = genHash(String.valueOf(SimpleDhtActivity.predecessor/2));
			String key_hash = genHash(selection);
			
			if((key_hash.compareTo(pred_hash) > 0 && key_hash.compareTo(node_hash) <= 0)
					|| (node_hash.compareTo(pred_hash) < 0 && key_hash.compareTo(pred_hash) > 0)
					|| (node_hash.compareTo(pred_hash) < 0 && key_hash.compareTo(node_hash) < 0)
					|| (node_hash.compareTo(succ_hash)==0)
					|| selection.equalsIgnoreCase("*")
					|| selection.equalsIgnoreCase("@")){
	    		
				/*Go inside if statement if query is not "*" or "@"*/
				
	        	if(!selection.equalsIgnoreCase("*") &&  !selection.equalsIgnoreCase("@")){
		        	StringBuffer buffer = new StringBuffer();
		        	int chr;
		        	FileInputStream fis;
	        		fis = getContext().openFileInput(selection);
	        		while((chr = fis.read()) != -1){
	        			buffer.append((char) chr);
	        		}
	        		fis.close();
	        		
					Log.d("Aman", "Origin is : " + SimpleDhtActivity.avd + " and current port is : " 
					+ SimpleDhtActivity.port_int + "File is : " + selection);
					
			    	if(SimpleDhtActivity.avd != SimpleDhtActivity.port_int){
			    		Log.d("Aman", "Found file : "+ selection +" in an other avd : " + SimpleDhtActivity.port_int);
			    		Message msg = new Message(SimpleDhtActivity.avd, selection, buffer.toString(),"RETURN");
			    		SendTask snd = new SendTask(msg);
			    		snd.start();
			    		SimpleDhtActivity.avd = SimpleDhtActivity.port_int;
			    		return null;
			    	}
			    	else{
			    		Log.d("Aman", "Found file : "+ selection + " in same avd : " + SimpleDhtActivity.port_int);
				    	Object[] tuple = new Object[cur.getColumnCount()];
				    	
				    	tuple[cur.getColumnIndex("key")] = selection;
				    	tuple[cur.getColumnIndex("value")] = buffer;
				    	
				    	cur.addRow(tuple);
				    	Log.v("query", selection);
				    	return cur;
			    	}
	        	}
	        	
	        	else {
        			String[] fileList = getContext().fileList();
        			
	        		if (selection.equalsIgnoreCase("@")){
	        			
	        			if(fileList.length == 0)
	        				return null;
	        			
	        			for(int i = 0; i < fileList.length; i++){
	    		        	StringBuffer buffer = new StringBuffer();
	    		        	int chr;
	    		        	FileInputStream fis = getContext().openFileInput(fileList[i]);
	    	        		while((chr = fis.read()) != -1){
	    	        			buffer.append((char) chr);
	    	        		}
	    	        		fis.close();
	    	        		
					    	Object[] tuple = new Object[cur.getColumnCount()];
					    	
					    	tuple[cur.getColumnIndex("key")] = fileList[i];
					    	tuple[cur.getColumnIndex("value")] = buffer;
					    	
					    	cur.addRow(tuple);  	
	        			}
	        			Log.v("query", selection);
				    	return cur;
	        		}
	        		else {
	        			
	        			ArrayList<String> keyValue = new ArrayList<String>();
	        			if(SimpleDhtActivity.avd != SimpleDhtActivity.port_int){
	        				keyValue.addAll(SimpleDhtActivity.starQuery);
	        				SimpleDhtActivity.starQuery.clear();
	        			}
	        			for(int i = 0; i < fileList.length; i++){
	        				StringBuffer buffer = new StringBuffer();
	    		        	int chr;
	    		        	FileInputStream fis = getContext().openFileInput(fileList[i]);
	    	        		while((chr = fis.read()) != -1){
	    	        			buffer.append((char) chr);
	    	        		}
	    	        		fis.close();
	    	        		String val = fileList[i] + "_" + buffer.toString();
	    	        		keyValue.add(val);
	        			}
	        			
	        			Log.d("Aman","Sending to "+SimpleDhtActivity.successor);
	        			
    	        		Message msg = new Message(SimpleDhtActivity.successor, SimpleDhtActivity.avd, keyValue, "STAR");
    	        		SendTask snd = new SendTask(msg);
    	        		snd.start();
    	        		
    	        		if(SimpleDhtActivity.avd == SimpleDhtActivity.port_int){
    	        			Log.d("Aman", SimpleDhtActivity.port_int + " holding for response");
    	        			SimpleDhtActivity.hold = true;
    	        			while(SimpleDhtActivity.hold){};
    	        			SimpleDhtActivity.starQuery.clear();
    	        			return SimpleDhtActivity.cur;
    	        		}
    	        		SimpleDhtActivity.avd = SimpleDhtActivity.port_int;	
	        		}       		
	        	}
	    	}
	    	else{
	    		Log.d("Aman", "Sending query request to " + SimpleDhtActivity.successor + 
	    				" for querying file : " + selection);
	    		Log.d("Aman", "For file : " + selection + " origin is  " + SimpleDhtActivity.avd 
	    				+ " with hold being " + SimpleDhtActivity.hold);
	    		Message msg = new Message(SimpleDhtActivity.successor, SimpleDhtActivity.avd, selection, "QUERY");
	    		SendTask snd = new SendTask(msg);
	    		snd.start();
	    		if(SimpleDhtActivity.avd == SimpleDhtActivity.port_int){
		    		SimpleDhtActivity.hold = true;
		    		while(SimpleDhtActivity.hold){};
		    		return SimpleDhtActivity.cur;
	    		}
	    		SimpleDhtActivity.avd = SimpleDhtActivity.port_int;
	    	}
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // TODO Auto-generated method stub
        return 0;
    }

    private String genHash(String input) throws NoSuchAlgorithmException {
        MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        byte[] sha1Hash = sha1.digest(input.getBytes());
        Formatter formatter = new Formatter();
        for (byte b : sha1Hash) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }
}
