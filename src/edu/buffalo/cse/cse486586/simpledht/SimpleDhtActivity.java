package edu.buffalo.cse.cse486586.simpledht;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Formatter;


import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.MatrixCursor;
import android.telephony.TelephonyManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class SimpleDhtActivity extends Activity {
	/*@author aman*/
	static final String TAG ="Aman" ;//SimpleDhtActivity.class.getSimpleName();
	static final int[] ring = {0, 0, 0, 0, 0};
	static final String[] REMOTE_PORT = {"11112", "11108", "11116", "11120", "11124"};
    static final int SERVER_PORT = 10000;
    static int predecessor, successor;
    static int port_int;
    static String node_id;
    static MatrixCursor cur;
    static boolean hold;
    static int avd;
    static ArrayList<String> starQuery = new ArrayList<String>();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_dht_main);
        
        try {
            ServerSocket serverSocket = new ServerSocket(SERVER_PORT);
            new serverTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverSocket);        
        } catch (IOException e) {
            Log.e(TAG, "Can't create a ServerSocket");
            return;
        }
        
        String portStr = getPort();
        port_int = 2 * Integer.parseInt(portStr);

        try {
			node_id = genHash(portStr);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        predecessor = port_int;
        successor = port_int;
        avd=port_int;
        
        if(port_int != 11108){
            Message msg = new Message(port_int, 0, 0, "RING_JOIN");
            new clientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, msg);
        }
        else{
        	ring[1] = 11108;
        }
        TextView tv = (TextView) findViewById(R.id.textView1);
        tv.setMovementMethod(new ScrollingMovementMethod());
        findViewById(R.id.button3).setOnClickListener(
                new OnTestClickListener(tv, getContentResolver()));
    }
    
    private class serverTask extends AsyncTask<ServerSocket, String, Void>{

		
    	@Override
		protected Void doInBackground(ServerSocket... sockets) {
			// TODO Auto-generated method stub
			
    		ServerSocket socket = sockets[0];
			ObjectInputStream inputstream;
			Message publish_message = null;
			
			while(true){
				try{					
					Socket server_sock = socket.accept();
					inputstream = new ObjectInputStream(server_sock.getInputStream());
					publish_message = (Message) inputstream.readObject();
					if(publish_message != null){
						int index = -1;
						int pred = -1;
						int succ = -1;
						if(publish_message.msg_type.equals("RING_JOIN")){
							Log.d("Aman", "Ring at the moment is : " + ring[0] + " " + ring[1] + " " 
									+ ring[2] + " " + ring[3] + " " + ring[4]);
							switch(publish_message.node){
							case 11112:
								ring[0] = 11112; index = 0;
								break;
							case 11116:
								ring[2] = 11116; index = 2;
								break;
							case 11120:
								ring[3] = 11120; index = 3;
								break;
							case 11124:
								ring[4] = 11124; index = 4;
								break;
							}
							int i = index;
							while(pred == -1){
								i--;
								if(i < 0)
									i = 4;
								
								if(ring[i] != 0)
									pred = ring[i];								
							}
							i = index;
							while(succ == -1){
								i++;
								if(i > 4)
									i = 0;
								
								if(ring[i] != 0)
									succ = ring[i];
							}
							//Update predecessor and successor of the joining node
							Message msg1 = new Message(ring[index], pred, succ, "UPDATE_NEIGHBOURS");
							new clientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, msg1);
							
							//Update predecessor of the successor of the joining node
							Message msg2 = new Message(succ, ring[index], 0, "UPDATE_NEIGHBOURS");
							new clientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, msg2);
							
							//Update successor of the predecessor of the joining node
							Message msg3 = new Message(pred, 0, ring[index], "UPDATE_NEIGHBOURS");
							new clientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, msg3);
						}
						else if(publish_message.msg_type.equals("UPDATE_NEIGHBOURS")){
							if(publish_message.pred != 0){
								predecessor = publish_message.pred;
							}
							if(publish_message.succ != 0){
								successor = publish_message.succ;
							}
						}
						else if(publish_message.msg_type.equals("INSERT")){
							Log.d("Aman", "Inside INSERT request in node : " + publish_message.node);
							Log.d("Aman", "Key is : " + publish_message.key + " Value is : " + publish_message.value);
							ContentResolver resolver = getContentResolver();
							Uri url = buildUri("content", "edu.buffalo.cse.cse486586.simpledht.provider");
							ContentValues cv = new ContentValues();
							cv.put("key", publish_message.key);
							cv.put("value", publish_message.value);
							resolver.insert(url, cv);
							Log.d("Aman", "Insertion has been done");
						}
						else if(publish_message.msg_type.equals("QUERY")){
							Log.d(TAG, "Inside QUERY request for : " + publish_message.key 
									+ "in node : " + publish_message.node);
							Log.d(TAG, "Origin is : " + publish_message.origin + 
									"for : " + publish_message.key );
							ContentResolver resolver = getContentResolver();
							Uri url = buildUri("content", "edu.buffalo.cse.cse486586.simpledht.provider");
							avd = publish_message.origin;
							resolver.query(url, null, publish_message.key, null, null);
						}
						else if(publish_message.msg_type.equals("RETURN")){
					    	Log.d("Aman", "Key : " + publish_message.key);
							String[] colNames = {"key", "value"};
					    	MatrixCursor get_cur = new MatrixCursor(colNames, 1);
					    	
					    	Object[] tuple = new Object[get_cur.getColumnCount()];
					    	tuple[get_cur.getColumnIndex("key")] = publish_message.key;
					    	tuple[get_cur.getColumnIndex("value")] = publish_message.value;
					    	get_cur.addRow(tuple);
					    	cur = get_cur;
					    	hold = false;
						}
						else if(publish_message.msg_type.equals("STAR")){
							starQuery.addAll(publish_message.kV);
							avd = publish_message.origin;
							if(avd != port_int){
								ContentResolver resolver = getContentResolver();
								Uri url = buildUri("content", "edu.buffalo.cse.cse486586.simpledht.provider");
								resolver.query(url, null, "*", null, null);
							}else{
								if(starQuery.size() == 0){
									Log.d(TAG, "No files present for response");
									cur = null;
								}
								else{
									String[] colNames = {"key", "value"};
							    	MatrixCursor get_cur = new MatrixCursor(colNames);
							    	for(int i = 0; i < starQuery.size(); i++){
							    		String[] key_value = starQuery.get(i).split("_");
							    		Object[] tuple = new Object[get_cur.getColumnCount()];
								    	tuple[get_cur.getColumnIndex("key")] = key_value[0];
								    	tuple[get_cur.getColumnIndex("value")] = key_value[1];
								    	get_cur.addRow(tuple);
							    	}
							    	cur = get_cur;
								}
						    	hold = false;
							}
						}
						else if(publish_message.msg_type.equals("DELETE")){
							ContentResolver resolver = getContentResolver();
							Uri url = buildUri("content", "edu.buffalo.cse.cse486586.simpledht.provider");
							resolver.delete(url,publish_message.key, null);
						}
					}
				}catch(IOException e){
            		e.printStackTrace();
                    break;
            	} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		}    	
    }
    
    private class clientTask extends AsyncTask<Message, Void, Void>{

		@Override
		protected Void doInBackground(Message... params) {
			// TODO Auto-generated method stub
			try{
				Message messageToSend = params[0];
				
				//Log.d(TAG, "In clienttask from : " + messageToSend.node);
				
				if(messageToSend.msg_type == "RING_JOIN"){
					Socket s = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}),
	                        Integer.parseInt(REMOTE_PORT[1]));
					ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
					out.writeObject(messageToSend);
					out.close();
					s.close();
				}
				else if (messageToSend.msg_type == "UPDATE_NEIGHBOURS"){
					Socket s = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}),
	                        messageToSend.node);
					ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
					out.writeObject(messageToSend);
					out.close();
					s.close();
				}
			}catch (UnknownHostException e) {
                Log.e(TAG, "ClientTask UnknownHostException");
            } catch (IOException e) {
                Log.e(TAG, "ClientTask socket IOException");
            }
			return null;
		}
    	
    }

    private String genHash(String input) throws NoSuchAlgorithmException {
        MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        byte[] sha1Hash = sha1.digest(input.getBytes());
        Formatter formatter = new Formatter();
        for (byte b : sha1Hash) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_simple_dht_main, menu);
        return true;
    }
    
    public String getPort(){
        TelephonyManager tel = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String portStr = tel.getLine1Number().substring(tel.getLine1Number().length() - 4);
        return portStr;
    }
    
	private Uri buildUri(String scheme, String authority) {
		Uri.Builder uriBuilder = new Uri.Builder();
		uriBuilder.authority(authority);
		uriBuilder.scheme(scheme);
		return uriBuilder.build();
	}

}
