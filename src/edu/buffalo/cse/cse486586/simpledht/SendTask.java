package edu.buffalo.cse.cse486586.simpledht;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class SendTask extends Thread {

	private Message msg;
	//private Thread t;
	
	public SendTask(Message m){
		msg = m;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Socket s = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}),msg.node);
			ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
			out.writeObject(msg);
			out.close();
			s.close();

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
