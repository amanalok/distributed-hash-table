package edu.buffalo.cse.cse486586.simpledht;

import java.io.Serializable;
import java.util.ArrayList;

public class Message implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int node;
	int pred;
	int succ;
    String msg_type;
    String key;
    String value;
    int origin;
    ArrayList<String> kV;
	
	public Message(int _n, int _p, int _s, String _type){
		node = _n;
		pred = _p;
		succ = _s;
		msg_type = _type;
	}
	
	public Message(int _n, String k, String v, String _type){
		node = _n;
		key = k;
		value = v;
		msg_type = _type;
	}
	
	public Message(int _n, int org, String k, String _type){
		node = _n;
		origin = org;
		key = k;
		msg_type = _type;
	}
	
	public Message(int _n, int org, ArrayList<String> _a, String _type){
		node = _n;
		origin = org;
		kV = _a;
		msg_type = _type;
	}
	
/*	public Message(int _n, String k, String _type){
		node = _n;
		key = k;
		msg_type = _type;
	}*/
}
